import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';
import { Box, Heading, Paragraph } from 'grommet';
import { useTranslation } from 'react-i18next';
import PresentationImage from '../../public/images/lofoten-cliff.jpg';

const SECTION_ID = 'presentation';

const PresentationBox = styled(Box)`
  height: 0;
  overflow: hidden;
  padding-top: 100vh;
  position: relative;
  text-shadow: black 0px 0px 10px, black 0px 0px 10px, black 0px 0px 10px, black 0px 0px 10px;
`;

const FullscreenBox = styled(Box)`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
`;

function PresentationSection() {
  const color = 'light-1';
  const { t: translation } = useTranslation();
  const introduction = translation([SECTION_ID, 'content', 'introduction'].join('.'));
  const subtitle = translation([SECTION_ID, 'content', 'subtitle'].join('.'));
  return (
    <PresentationBox id="presentation">
      <FullscreenBox>
        <Image
          alt="Presentation background"
          layout="fill"
          objectFit="cover"
          objectPosition="center bottom"
          priority
          quality={100}
          src={PresentationImage.src}
          style={{ zIndex: -1 }}
        />
      </FullscreenBox>
      <FullscreenBox
        direction="column"
        justify="end"
        pad="large"
      >
        <Paragraph color={color}>{introduction}</Paragraph>
        <Heading color={color} margin="none" responsive>Laurie Chan Lam</Heading>
        <Paragraph color={color}>{subtitle}</Paragraph>
      </FullscreenBox>
    </PresentationBox>
  );
}

export default PresentationSection;
