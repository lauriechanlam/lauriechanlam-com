import { ColorType } from 'grommet/utils';
import { IconProps } from 'grommet-icons';
import { ThemeContext, ThemeValue } from 'grommet';
import { get, isString, isUndefined } from 'lodash-es';
import React from 'react';
import Logo from '../assets/images/gitlab.svg';

function getColorFromTheme(theme: ThemeValue, color: string | undefined): string | undefined {
  if (isUndefined(color)) {
    return undefined;
  }
  const colorType = get(theme, `global.colors.${color}`, color) as ColorType;
  if (isUndefined(colorType) || isString(colorType)) {
    return colorType;
  }
  return colorType.dark;
}

function getSize(size: string | undefined): string | undefined {
  switch (size || 'medium') {
    case 'small':
      return '12px';
    case 'medium':
      return '24px';
    case 'large':
      return '48px';
    case 'xlarge':
      return '96px';
    default:
      return size;
  }
}

function GitLab({ a11yTitle, color, size }: IconProps) {
  return (
    <ThemeContext.Consumer>
      {(theme) => (
        <Logo
          aria-label={a11yTitle}
          fill="none"
          stroke={getColorFromTheme(theme, color)}
          strokeWidth="16"
          width={getSize(size)}
          height={getSize(size)}
        />
      )}
    </ThemeContext.Consumer>
  );
}

GitLab.defaultProps = {
  a11yTitle: 'Gitlab',
  color: undefined,
  size: undefined,
};

export default GitLab;
