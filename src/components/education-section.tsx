import React from 'react';

import {
  BackgroundType,
  DirectionType,
  HeightType,
  WidthType,
} from 'grommet/utils';
import {
  Box,
  Heading,
  ResponsiveContext,
  Tab,
} from 'grommet';
import { get } from 'lodash-es';
import { useTranslation } from 'react-i18next';

import Anchor from './anchor';
import Markdown from './markdown';
import Section from './section';
import Tabs from './tabs';
import config from '../assets/content/content.json';

const SECTION_ID = 'education';

type Education = {
  id: string;
  end: number;
  location: string;
  start: number;
  university: { name: string, link: string };
};

function EducationDisplay({
  accentColor, id, end, location, start, university,
}: { accentColor?: string } & Education) {
  const { t: translation } = useTranslation();
  const title = translation([SECTION_ID, 'content', 'education', id, 'title'].join('.'));
  const description = translation([SECTION_ID, 'content', 'education', id, 'description'].join('.'));
  return (
    <Box gap="small" pad="small">
      <Heading level={3} margin="none" fill responsive>
        {`${title} - `}
        <Anchor color={accentColor} href={university.link} label={university.name} />
      </Heading>
      <Heading level={4} margin="none" responsive>
        {`${location} ${start} - ${end}`}
      </Heading>
      <Markdown accentColor={accentColor} content={description} />
    </Box>
  );
}

EducationDisplay.defaultProps = {
  accentColor: undefined,
};

function EducationSection({ accentColor, background }: { accentColor?: string; background?: BackgroundType }) {
  const education = get(config, `${SECTION_ID}.content`) as Education[];
  return (
    <Section accentColor={accentColor} background={background} id={SECTION_ID}>
      <ResponsiveContext.Consumer>
        {(responsive) => {
          const { direction, height, width }: {
            direction: DirectionType;
            height?: HeightType;
            width?: WidthType;
          } = responsive === 'large'
            ? { direction: 'column', height: undefined, width: 'small' }
            : { direction: 'row', height: undefined, width: undefined };

          return (
            <Tabs direction={direction} height={height} width={width}>
              {education.map(({
                id, end, location, start, university,
              }) => (
                <Tab key={id} a11yTitle={university.name} title={university.name}>
                  <EducationDisplay
                    id={id}
                    accentColor={accentColor}
                    end={end}
                    location={location}
                    start={start}
                    university={university}
                  />
                </Tab>
              ))}
            </Tabs>
          );
        }}
      </ResponsiveContext.Consumer>

    </Section>
  );
}

EducationSection.defaultProps = {
  accentColor: undefined,
  background: undefined,
};

export default EducationSection;
