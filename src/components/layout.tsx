import React, { useEffect, useMemo, useState } from 'react';

import { Grommet } from 'grommet';
import {
  get, has, sortBy, toPairs,
} from 'lodash-es';

import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import Head from './head';
import Loader from './loader';
import MenuBar from './menu-bar';
import config from '../assets/content/content.json';
import theme from '../themes/base';

type LayoutProps = {
  children: React.ReactNode;
};

const NoScrollbarGrommet = styled(Grommet)`
  -ms-overflow-style: none;
  scrollbar-width: none;

  &::-webkit-scrollbar {
    display: none;
  }
`;

function Layout({ children }: LayoutProps) {
  const { t: translation } = useTranslation();
  const navigations = useMemo(() => {
    const sectionIds = sortBy(
      toPairs(config)
        .filter(([, value]) => has(value, 'index')),
      ([, value]) => get(value, 'index'),
    )
      .map(([id]) => id);
    return sectionIds.map((sectionId) => ({
      a11yTitle: translation([sectionId, 'navigation', 'a11yTitle'].join('.')),
      id: sectionId,
      label: translation([sectionId, 'navigation', 'label'].join('.')),
    }));
  }, [translation]);
  const ref = React.createRef<HTMLDivElement>();
  const [isLoaded, setLoaded] = useState(false);
  useEffect(() => {
    if (ref.current !== undefined) {
      setLoaded(true);
    }
  }, [ref]);
  return isLoaded ? (
    <NoScrollbarGrommet full id="container" ref={ref} theme={theme}>
      <Head />
      <MenuBar
        background="brand"
        containerRef={ref}
        items={navigations}
      />
      {children}
    </NoScrollbarGrommet>
  ) : <Loader />;
}

export default Layout;
