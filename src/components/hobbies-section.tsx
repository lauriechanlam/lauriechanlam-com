import React from 'react';

import { BackgroundType, ColorType } from 'grommet/utils';
import {
  Box,
  Heading,
  ResponsiveContext,
  Text,
} from 'grommet';
import { get } from 'lodash-es';
import { useTranslation } from 'react-i18next';

import AvatarIcon from './avatar-icon';
import Section from './section';
import config from '../assets/content/content.json';

const SECTION_ID = 'hobbies';

type Hobby = { id: string, icon: string };

function HobbyDisplay({ id, icon }: Hobby) {
  const { t: translation } = useTranslation();
  const name = translation([SECTION_ID, 'content', 'hobbies', id, 'name'].join('.'));
  const list: string[] = translation([SECTION_ID, 'content', 'hobbies', id, 'list'].join('.'));
  return (
    <Box key={id} direction="column" align="center" width="100%">
      <AvatarIcon a11yTitle={name} background="brand" icon={icon} size="large" />
      <Heading level={3} responsive>{name}</Heading>
      {list.map((hobby) => <Text key={hobby} style={{ whiteSpace: 'nowrap' }}>{hobby}</Text>)}
    </Box>
  );
}

function HobbiesSection({ accentColor, background }: { accentColor?: ColorType, background?: BackgroundType }) {
  const hobbies = get(config, `${SECTION_ID}.content`) as Hobby[];
  return (
    <Section accentColor={accentColor} background={background} id={SECTION_ID}>
      <ResponsiveContext.Consumer>
        {(responsive) => (
          <Box direction={responsive === 'large' ? 'row' : 'column'} gap="xlarge">
            {hobbies.map(({ id, icon }) => <HobbyDisplay key={id} id={id} icon={icon} />)}
          </Box>
        )}

      </ResponsiveContext.Consumer>
    </Section>
  );
}

HobbiesSection.defaultProps = {
  accentColor: undefined,
  background: undefined,
};

export default HobbiesSection;
