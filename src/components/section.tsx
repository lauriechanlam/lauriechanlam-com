import React from 'react';

import { Box, Heading } from 'grommet';
import { BackgroundType, ColorType, HeightType } from 'grommet/utils';
import { Element } from 'react-scroll';
import { useTranslation } from 'react-i18next';

type SectionProps = {
  accentColor?: ColorType;
  background?: BackgroundType;
  children: React.ReactNode;
  height?: HeightType;
  id: string;
};

function Section({
  accentColor, background, children, height, id,
}: SectionProps) {
  const { t: translation } = useTranslation();
  return (
    <Element name={id}>
      <Box align="center" background={background} height={height} gap="medium" pad="large">
        <Heading
          color={accentColor}
          level={2}
          margin="none"
          responsive
        >
          {translation([id, 'content', 'title'].join('.'))}
        </Heading>
        {children}
      </Box>
    </Element>
  );
}

Section.defaultProps = {
  accentColor: undefined,
  background: undefined,
  height: undefined,
};

export default Section;
