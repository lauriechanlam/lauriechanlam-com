import React from 'react';

import { BackgroundType } from 'grommet/utils';
import { Box, Heading } from 'grommet';
import {
  Linkedin, Mail, Notes, Twitter,
} from 'grommet-icons';
import { useTranslation } from 'react-i18next';

import Anchor from './anchor';
import GitLab from './gitlab';
import Section from './section';
import i18n from '../i18n/i18n';

const SECTION_ID = 'contact';

function ContactSection({ background, color }: { background?: BackgroundType; color?: string }) {
  const { t: translation } = useTranslation();
  const text = translation([SECTION_ID, 'content', 'text'].join('.'));
  const lang = i18n.language.slice(0, 2);
  return (
    <Section accentColor={color} background={background} id={SECTION_ID}>
      <Heading color={color} level={3} responsive>{text}</Heading>
      <Box direction="row" gap="large">
        <Anchor href="https://www.linkedin.com/in/lauriechanlam/?locale=en_US">
          <Linkedin color={color} size="large" />
        </Anchor>
        <Anchor href="https://twitter.com/lauriechanlam"><Twitter color={color} size="large" /></Anchor>
        <Anchor href="mailto:laurie@lauriechanlam.com"><Mail color={color} size="large" /></Anchor>
        <Anchor href="https://gitlab.com/lauriechanlam"><GitLab color={color} size="large" /></Anchor>
        <Anchor href={`../CV-laurie-chan-lam-${lang}.pdf`}><Notes color={color} size="large" /></Anchor>
      </Box>
    </Section>
  );
}

ContactSection.defaultProps = {
  background: undefined,
  color: undefined,
};

export default ContactSection;
