import NextHead from 'next/head';
import React from 'react';
import { NextSeo } from 'next-seo';
import { isUndefined } from 'lodash-es';
import i18n from '../i18n/i18n';

function Head() {
  const locale = i18n.language;
  const host = process.env.NEXT_PUBLIC_HOST;
  if (isUndefined(host)) {
    throw new Error('Missing environment variable NEXT_PUBLIC_HOST');
  }
  return (
    <>
      <NextSeo
        titleTemplate="Laurie Chan Lam | %s"
        defaultTitle="Laurie Chan Lam"
        description="Laurie Chan Lam's awsome website"
        openGraph={{
          url: host,
          type: 'website',
          title: 'Laurie Chan Lam',
          description: "Laurie Chan Lam's awsome website",
          images: [
            {
              url: `${host}/images/lofoten-cliff-thumbnail.jpg`,
            },
            {
              url: `${host}/images/avatar.jpg`,
            },
          ],
          locale,
          site_name: host,
          profile: {
            firstName: 'Laurie',
            lastName: 'Chan Lam',
            username: 'lauriechanlam',
          },
        }}
        twitter={{
          handle: '@lauriechanlam',
          site: host,
          cardType: 'summary',
        }}
      />
      <NextHead>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Laurie Chan Lam</title>
      </NextHead>
    </>
  );
}

export default Head;
