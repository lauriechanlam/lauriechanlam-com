import { ThemeType } from 'grommet';

const theme: ThemeType = {
  global: {
    colors: {
      brand: '#cedcea',
      'accent-1': '#265155',
      'accent-2': '#323d49',
      'accent-3': {
        dark: '#ff9eb6',
        light: '#8d0b2c',
      },
      text: {
        dark: 'brand',
        light: 'accent-1',
      },
    },
    font: {
      family: 'Raleway',
    },
    breakpoints: {
      medium: { value: 980 },
    },
    focus: {
      border: {
        color: 'text',
      },
    },
  },
};

export default theme;
