const { transform } = require('@babel/core');

module.exports = {
  process(src, filename) {
    const result = transform(src, {
      filename,
      presets: [
        require('@babel/preset-env'),
        require('@babel/preset-typescript'),
      ],
    });

    return result || src;
  },
};
