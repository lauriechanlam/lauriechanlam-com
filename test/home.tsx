import { render } from '@testing-library/react';
import React from 'react';
import '../src/i18n/i18n';
import Home from '../src/components/home';

describe('Home', () => {
  it('renders the home page', async () => {
    const { findAllByRole } = render(<Home />);
    const elements = await findAllByRole('heading', { name: 'Laurie Chan Lam' });
    expect(elements.length).toBeGreaterThan(0);
    elements.map((element) => expect(element).toBeInTheDocument());
  });
});
